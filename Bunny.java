public class Bunny{
	private boolean isCute;
	private int numOfEars;
	private String favoriteFood;
	
	public Bunny(boolean isCute, int numOfEars, String favoriteFood){
		this.isCute = isCute;
		this.numOfEars = numOfEars;
		this.favoriteFood = favoriteFood;
	}
	
	public void shouldHunt(){
		if (this.isCute){
			System.out.println("I should not hunt this bunny. it's too cute!");
		}
		else System.out.println("I should hunt this bunny. it's ugly!");
	}
	public void giveFood(){
		System.out.println("I gave the bunny some "+this.favoriteFood+".");
	}
	
	public boolean getIsCute(){
		return this.isCute;
	}
	public int getNumOfEars(){
		return this.numOfEars;
	}
	public String getFavoriteFood(){
		return this.favoriteFood;
	}
	
	public void setIsCute(boolean newIsCute){
		this.isCute = newIsCute;
	}
}