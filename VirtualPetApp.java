import java.util.Scanner;
public class VirtualPetApp{
	
	public static void main(String[] args){
		
		Bunny[] bunColony= new Bunny[1];
		Scanner sc= new Scanner(System.in);
		for (int i=0;i<bunColony.length;i++){
			boolean isCute;
			int numberOfEars;
			String favFood;
			
			System.out.println("is bunny "+(i+1)+" cute? type 1 for yes and 0 for no");
			int playerChoice= Integer.parseInt(sc.nextLine());
			if (playerChoice==1){
				isCute = true;
			}
			else isCute = false;
			
			System.out.println("How many ears does it have?");
			numberOfEars = Integer.parseInt(sc.nextLine());
			
			System.out.println("What is the bunny's favorite food?");
			favFood = sc.nextLine();
			
			bunColony[i]= new Bunny(isCute, numberOfEars, favFood);
		}
		System.out.println(bunColony[bunColony.length -1].getIsCute());
		System.out.println(bunColony[bunColony.length -1].getNumOfEars());
		System.out.println(bunColony[bunColony.length -1].getFavoriteFood());
		
		System.out.println("is the last bunny cute? 1 for true, 2 for fales");
		int isTheLastBunnyCute= Integer.parseInt(sc.nextLine());
		if (isTheLastBunnyCute==1){
				bunColony[bunColony.length -1].setIsCute(true);
			}
			else bunColony[bunColony.length -1].setIsCute(false);
			
		System.out.println(bunColony[bunColony.length -1].getIsCute());
		System.out.println(bunColony[bunColony.length -1].getNumOfEars());
		System.out.println(bunColony[bunColony.length -1].getFavoriteFood());
	}
}